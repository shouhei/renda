#coding: utf-8
from flask import Flask, render_template, session
from model import Model
app = Flask(__name__)

@app.route("/") 
def index():
    model = Model()
    id = model.insert_user()
    #return jsonify(res='ok')
    return render_template("index.html",id=id[0])
 
@app.route("/renda/<int:id>")
def renda(id):
    return render_template("renda.html",id=id)

@app.route("/insert_result/<int:id>/<int:result>")
def insert_result(id,result):
    model = Model()
    temp_id =  unicode(str(id))
    temp_result = unicode(str(result))
    result = model.insert_result(temp_id,temp_result)
    return render_template("result.html",result=result)

@app.route("/omikuzi/<int:id>/<int:result>")
def omikuzi(id,result):
    num = result / 50
    temp_num =  unicode(str(num))
    return render_template("slot.html",num=num,id=id)

@app.route("/test/<int:id>/<int:result>")
def test(id,result):
    temp_id =  unicode(str(id))
    temp_result = unicode(str(result))
    return render_template("test.html",id=temp_id,result=temp_result)

@app.route("/ranking/<int:id>")
def ranking(id):
    temp_id =  unicode(str(id))
    model = Model()
    ranking = model.ranking(temp_id)
    return render_template("ranking.html",ranking=ranking)
    

app.run(debug=True)

