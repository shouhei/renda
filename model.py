#coding:utf-8

from flask import Flask
import sqlite3

class Model:
    name = ""
    con = ""

    def __init__(self):
        self.name = 'SuperClass'
        self.con = sqlite3.connect('data.db', isolation_level=None) 

    def insert_user(self):
        sql = u"insert into user(result) values(null)"
        self.con.execute(sql)
        c = self.con.cursor()
        c.execute(u"select id from user order by id desc")
        result = c.fetchone()
        return result

    def insert_result(self,id,result):
        insert = u"update user set result=" + result + u" where id=" + id
        self.con.execute(insert)
        c = self.con.cursor()
        c.execute(u"select * from user where id=" + id + u" order by id desc")
        tmp = c.fetchone()
        return tmp

    def ranking(self,id):
        c = self.con.cursor()
        query = u"select P1.id, P1.result, (select count(*) from user as P2 where P2.result > P1.result) + 1 as rank_1 from user as P1 where id="+ id +" order by rank_1;"
        c.execute(query)
        tmp = c.fetchone()
        return tmp

    def end_model(self):
        self.con.close()
